# Gitter IRC Bridge

[![Join the chat at https://gitter.im/gitterHQ/irc-bridge](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/gitterHQ/irc-bridge?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Connect to Gitter using the IRC protocol.

## Configuring your client

You can connect to the **official** Gitter IRC bridge at:

 - host: `irc.gitter.im`
 - port: `6667` (or `6697`)

**SSL is mandatory**

You'll need to provide a valid token to authenticate yourself. You can send your token to the server using the `/PASS` command. If you use a GUI client, this is the Server Password; your client will send it automatically on connection.

You can obtain your token from https://irc.gitter.im

You can also find instructions on [how to configure specific clients in the Wiki](https://github.com/gitterHQ/irc-bridge/wiki/Client-configuration)

## Contributing

1. Fork the project.
1. Make your feature addition or bug fix.
1. Add tests for it. This is important so it doesn't break it in a future version unintentionally.
1. Add documentation if necessary.
1. Commit. Do not mess with the version or history.
1. Send a Merge Request! :+1:

## Running the bridge locally

You'll need a working Node.js environment.

 * Install the dependencies with: `npm install`
 * Start the server with: `DEBUG=irc* npm start`
 * The local IRC server connects to production Gitter (unless `DEV=true` environment variable is set)
 * Connect to localhost IRC server without SSL, using your production access token (you can get it here https://irc.gitter.im/)
     * e.g. in `weechat` run `/server add localhost -password=<your token>`

## Infrastructure

The IRC bridge is deployed to 2 environments: beta and production

```mermaid
graph LR
A[URL irc-beta.gitter.im] --> B[process living on gitter-beta-01.beta.gitter]
B --> X[production API - https://gitter.im]
C[URL irc.gitter.im] --> D[processes living on apps-0X.prod.gitter]
D --> X
```

## Release and deployment

1. When all changes are in `develop`, start new release `git flow release 1.9.0 start`
1. Bump up the version on release branch `npm --no-git-tag-version version 1.9.0`
1. Push the release branch to origin `git flow release 1.9.0 publish`
1. Finish the release with `git flow release 1.9.0 finish`
1. Push your local `master`, `develop` and `--tags` to origin
1. Validate the bridge works in beta (`irc-beta.gitter.im`)
1. Run the `deploy_prod_manual` job in CI on the new tag
1. Validate the bridge works in production (`irc.gitter.im`)
